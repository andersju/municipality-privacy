import sys
from automation import TaskManager, CommandSequence

NUM_BROWSERS = 1

# Load the sites of sites we we wish to crawl into a list
# E.g. ['http://www.example.com', 'http://dataskydd.net']
sites = [line.rstrip('\n') for line in open('municipalities_final_urls.txt')]

# Loads the manager preference and 1 copy of the default browser dictionary
manager_params, browser_params = TaskManager.load_default_params(NUM_BROWSERS)

# Update browser configuration (use this for per-browser settings)
for i in xrange(NUM_BROWSERS):
    browser_params[i]['headless'] = True #Launch browser headless

# Update TaskManager configuration (use this for crawl-wide settings)
manager_params['data_directory'] = './data/'
manager_params['log_directory'] = './data/'

# Instantiates the measurement platform
# Commands time out by default after 60 seconds
manager = TaskManager.TaskManager(manager_params, browser_params)

# Visits the sites with all browsers simultaneously
for site in sites:
    command_sequence = CommandSequence.CommandSequence(site)
    command_sequence.browse(num_links=5, sleep=10, timeout=360)
    command_sequence.dump_profile_cookies(120)
    manager.execute_command_sequence(command_sequence, index='**') # ** = synchronized browsers

# Shuts down the browsers and waits for the data to finish logging
manager.close()
